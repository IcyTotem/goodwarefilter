package it.polimi.elet.necst.goodware.pipeline;

import it.polimi.elet.necst.goodware.apk.DecodedPackage;
import it.polimi.elet.necst.goodware.apk.DecodingException;
import it.polimi.elet.necst.goodware.apk.PackageDecoder;
import it.polimi.elet.necst.goodware.apk.PackageDecoders;
import it.polimi.elet.necst.goodware.utils.FileSystem;
import it.polimi.elet.necst.goodware.xml.ParsingException;
import it.polimi.elet.necst.goodware.xml.manifest.ManifestAnalysisReport;
import it.polimi.elet.necst.goodware.xml.manifest.ManifestAnalyzer;
import it.polimi.elet.necst.goodware.xml.manifest.ManifestAnalyzers;
import it.polimi.elet.necst.goodware.xml.resources.StringResource;
import it.polimi.elet.necst.goodware.xml.resources.StringResourceMetaParser;
import it.polimi.elet.necst.goodware.xml.resources.StringResourceParsers;

import java.io.File;

public class ApplicationData {
    private static PackageDecoder decoder;
    private static StringResourceMetaParser parser;
    private static ManifestAnalyzer analyzer;

    private DecodedPackage decodedPackage;
    private ManifestAnalysisReport manifestReport;
    private StringResource stringResource;

    public DecodedPackage getDecodedPackage() {
        return decodedPackage;
    }

    public ManifestAnalysisReport getManifestReport() {
        return manifestReport;
    }

    public StringResource getStringResource() {
        return stringResource;
    }

    private ApplicationData() { }

    public static ApplicationData open(File file) throws ParsingException, DecodingException {
        if (isApkFile(file))
            return extract(file);

        if (isUnpackedApkDirectory(file))
            return read(file);

        throw new DecodingException(file.getAbsolutePath() + " is not a valid android package.");
    }

    private static boolean isUnpackedApkDirectory(File directory) {
        File manifest = new File(directory, "AndroidManifest.xml");
        return manifest.exists();
    }

    private static boolean isApkFile(File file) {
        return file.getName().toLowerCase().endsWith(".apk");
    }

    private static ApplicationData extract(File apk) throws DecodingException, ParsingException {
        DecodedPackage decodedPackage;

        if (decoder == null)
            decoder = PackageDecoders.saaf();

        decodedPackage = decoder.decode(apk);

        return process(decodedPackage);
    }

    private static ApplicationData read(File unpackedApkDirectory) throws ParsingException {
        final File mainDirectory = unpackedApkDirectory;
        DecodedPackage decodedPackage = new DecodedPackage() {
            @Override
            public File getClassesDex() {
                return new File(mainDirectory, "classes.dex");
            }

            @Override
            public File getAndroidManifest() {
                return new File(mainDirectory, "AndroidManifest.xml");
            }

            @Override
            public File getDecodedDirectory() {
                return mainDirectory;
            }

            @Override
            public File getSmaliDirectory() {
                return new File(mainDirectory, "smali");
            }

            @Override
            public File getOriginalApk() {
                return mainDirectory;
            }

            @Override
            public void dispose() { /* Do nothing, since directory is not created by the application */ }
        };

        return process(decodedPackage);
    }

    private static ApplicationData process(DecodedPackage decodedPackage) throws ParsingException {
        ApplicationData result = new ApplicationData();

        result.decodedPackage = decodedPackage;

        if (parser == null)
            parser = new StringResourceMetaParser(StringResourceParsers.domBased());

        result.stringResource = parser.parseDirectory(decodedPackage.getDecodedDirectory());

        if (analyzer == null)
            analyzer = ManifestAnalyzers.domBased(result.stringResource);

        result.manifestReport = analyzer.analyze(decodedPackage.getAndroidManifest());

        return result;
    }

    public void dispose() {
        decodedPackage.dispose();
    }
}
