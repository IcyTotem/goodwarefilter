package it.polimi.elet.necst.goodware;

import it.polimi.elet.necst.goodware.apk.DecodingException;
import it.polimi.elet.necst.goodware.filters.*;
import it.polimi.elet.necst.goodware.filters.core.Filter;
import it.polimi.elet.necst.goodware.filters.features.Feature;
import it.polimi.elet.necst.goodware.pipeline.ApplicationData;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {
    private static final String EXAMINED_FILES_LIST_NAME = "examined.txt";
    private static final int THREADS_COUNT = 10;

    private static List<String> examinedFiles;
    private static boolean featuresHeaders;
    private static MetaFilter metaFilter;
    private static BufferedWriter matchesWriter, featuresWriter, examinedFilesWriter;
    private static ExecutorService executor;

    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length < 3)
            return;

        File target = new File(args[0]);

        metaFilter = new MetaFilter();
        metaFilter.add(new DangerousPermissionsFilter());
        metaFilter.add(new DangerousApiFilter());
        //metaFilter.add(new SystemCallsFilter());
        metaFilter.add(new HiddenApkFilter());
        metaFilter.add(new PotentialLeakageFilter());
        //metaFilter.add(new SmsNumbersFilter());
        metaFilter.add(new AirpushFilter());
        metaFilter.add(new PlayStorePermissionsFilter());
        metaFilter.add(new PlayStorePopularityFilter());
        metaFilter.add(new ValidDomainFilter());
        metaFilter.add(new PackageNameFilter());
        metaFilter.add(new NativeLibrariesFilter());

        initExaminedFiles();
        initMatchesWriter(args[1]);
        initFeaturesWriter(args[2]);

        if (target.isDirectory()) {
            executor = Executors.newFixedThreadPool(THREADS_COUNT);
            scanDirectory(target);
        } else
            scan(target);

        if (executor != null) {
            executor.shutdown();
            executor.awaitTermination(1, TimeUnit.HOURS);
        }

       closeWriters();
    }

    private static void initExaminedFiles() throws IOException {
        File repo = new File(EXAMINED_FILES_LIST_NAME);

        examinedFiles = new ArrayList<String>();

        if (repo.exists()) {
            BufferedReader reader = new BufferedReader(new FileReader(repo));
            String line;

            while ((line = reader.readLine()) != null)
                examinedFiles.add(line);

            reader.close();
        }

        examinedFilesWriter = new BufferedWriter(new FileWriter(repo, true)); // append to repo, if it exists
    }

    private static void initMatchesWriter(String filePath) throws IOException {
        File repo = new File(filePath);
        boolean headersNeeded = !repo.exists();

        matchesWriter = new BufferedWriter(new FileWriter(repo, true)); // append

        if (headersNeeded) {
            matchesWriter.write("ApkName");
            for (Filter filter : metaFilter.getFilters())
                matchesWriter.write(", " + filter.getClass().getSimpleName());
            matchesWriter.newLine();
        }
    }

    private static void initFeaturesWriter(String filePath) throws IOException {
        File repo = new File(filePath);

        featuresHeaders = repo.exists(); // if file already exists, no need to write headers
        featuresWriter = new BufferedWriter(new FileWriter(repo, true)); // append
    }

    private static void scanDirectory(File directory) {
        for (File file : directory.listFiles()) {
            scan(file);

            if (file.isDirectory())
                scanDirectory(file);
        }
    }

    private static void scan(File file) {
        synchronized (examinedFiles) {
            if (examinedFiles.contains(file.getAbsolutePath())) {
                System.out.println("Skipped: " + file.getName());
                return;
            }
        }

        final File fileReference = file;

        if (executor == null) {
            scanData(fileReference);
        } else {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    scanData(fileReference);
                }
            });
        }
    }

    private static void scanData(File file) {
        ApplicationData applicationData;

        System.out.println("Submitted: " + file.getName());

        try {
            applicationData = ApplicationData.open(file);
        } catch (Exception e) {
            System.out.println("Dropped: " + e.getMessage());
            return;
        }

        String apkName = applicationData.getDecodedPackage().getOriginalApk().getName();

        Map<Filter, Boolean> matches = metaFilter.matchAllFilters(applicationData);
        Collection<Feature> features = metaFilter.getAllFiltersFeatures();

        writeMatches(apkName, matches);
        writeFeatures(apkName, features);
        registerFileAsExamined(applicationData.getDecodedPackage().getOriginalApk());

        applicationData.dispose();

        System.out.println("Completed: " + apkName);
    }

    private static void registerFileAsExamined(File file) {
        synchronized (examinedFiles) {
            examinedFiles.add(file.getAbsolutePath());
        }

        synchronized (examinedFilesWriter) {
            try {
                examinedFilesWriter.write(file.getAbsolutePath());
                examinedFilesWriter.newLine();
                examinedFilesWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void writeMatches(String apkName, Map<Filter, Boolean> matches) {
        synchronized (matchesWriter) {
            try {
                matchesWriter.write(apkName);

                for (Filter filter : metaFilter.getFilters())
                    matchesWriter.write(", " + String.valueOf(matches.get(filter)));

                matchesWriter.newLine();
                matchesWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void writeFeatures(String apkName, Collection<Feature> features) {
        synchronized (featuresWriter) {
            try {
                if (!featuresHeaders) {
                    featuresWriter.write("ApkName");

                    for (Feature feature : features)
                        featuresWriter.write(", " + feature.getName());

                    featuresWriter.newLine();
                    featuresHeaders = true;
                }

                featuresWriter.write(apkName);

                for (Feature feature : features)
                    featuresWriter.write(", " + feature.toString());


                featuresWriter.newLine();
                featuresWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void closeWriters() throws IOException {
        matchesWriter.close();
        featuresWriter.close();
        examinedFilesWriter.close();
    }
}
