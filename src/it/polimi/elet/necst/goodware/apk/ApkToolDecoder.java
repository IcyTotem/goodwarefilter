package it.polimi.elet.necst.goodware.apk;

import java.io.File;

/**
 * Created by Nicolo on 04/02/14.
 */
class ApkToolDecoder implements PackageDecoder {
    private static final String APK_EXTENSION = ".apk";

    // Decode is the action that makes apktool unpack an apk into its components
    private static String action = "decode";

    // -s allows us to have a single classes.dex file rather than many .smali files
    // -r excludes decoding of resources (this includes also the application manifest)
    // -f forces overwriting of the output directory if it already exists
    private static String[] options = { "-f" };

    private File outputDirectory;

    public ApkToolDecoder(File outputDirectory) {
        this.outputDirectory = outputDirectory;
    }

    @Override
    public DecodedPackage decode(File apkFile) throws DecodingException {
        if (!apkFile.getName().endsWith(APK_EXTENSION))
            throw new DecodingException("Invalid file type.");

        String[] apkToolArguments = new String[2 + options.length + 2];
        int i = 0;

        // Apktool supports two main actions: build and decode
        apkToolArguments[i++] = "-q";
        apkToolArguments[i++] = action;

        // Plus a number of options
        for (int j = 0 ; j < options.length; j++)
            apkToolArguments[i++] = options[j];

        // Then the path to the apk to be processed
        apkToolArguments[i++] = apkFile.getAbsolutePath();

        String apkName = apkFile.getName();
        File workingDirectory = new File(outputDirectory, apkName);

        // And finally the directory to work in
        apkToolArguments[i] = workingDirectory.getAbsolutePath();

        try {
            brut.apktool.Main.main(apkToolArguments);
            return new ApkToolOutput(apkFile, workingDirectory);
        } catch (Exception e) {
            throw new DecodingException(e);
        }
    }
}
