package it.polimi.elet.necst.goodware.apk;

import java.io.File;

public class PackageDecoders {
    private static ApkToolDecoder apkToolDecoder;
    private static SaafDecoder saafDecoder;

    public static PackageDecoder apkTool() {
        if (apkToolDecoder != null)
            return apkToolDecoder;

        String tempDirectory = System.getProperty("java.io.tmpdir");
        apkToolDecoder = new ApkToolDecoder(new File(tempDirectory));

        return apkToolDecoder;
    }

    public static PackageDecoder saaf() {
        if (saafDecoder != null)
            return saafDecoder;

        String tempDirectory = System.getProperty("java.io.tmpdir");
        saafDecoder = new SaafDecoder(new File(tempDirectory));

        return saafDecoder;
    }
}
