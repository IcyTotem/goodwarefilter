package it.polimi.elet.necst.goodware.apk;

import it.polimi.elet.necst.goodware.utils.Xml;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.Collection;

public class SaafReport {
    public interface AnalysisTask {
        void perform(Element element);
    }

    private static final String REPORTS_DIRECTORY_NAME = "reports";
    private static final String REPORT_NAME_PREFIX = "Report-";
    private static final String REPORT_EXTENSION = ".xml";
    private static final String BACKTRACK_TAG_NAME = "backtrack-result";

    private static DocumentBuilderFactory dbFactory;

    private File file;
    private Document xmlDocument;
    private boolean xmlParsed;

    static {
        dbFactory = DocumentBuilderFactory.newInstance();
    }

    public File getFile() {
        return file;
    }

    public Document getXmlDocument() {
        if ((xmlDocument != null) || xmlParsed)
            return xmlDocument;

        try {
            DocumentBuilder db = dbFactory.newDocumentBuilder();
            Document document = db.parse(this.file);

            document.getDocumentElement().normalize();

            return (xmlDocument = document);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            xmlParsed = true;
        }

        return null;
    }

    private SaafReport(File file) {
        this.file = file;
        this.xmlParsed = false;
    }

    public void analyzeBacktracks(AnalysisTask task) {
        Document document = this.getXmlDocument();

        if (document == null)
            return;

        Collection<Element> backtrackElements = Xml.getElementsByTagName(document.getDocumentElement(), BACKTRACK_TAG_NAME);

        for (Element backtrackElement : backtrackElements)
            task.perform(backtrackElement);
    }


    private static SaafReport find(File apk) {
        String reportName = REPORT_NAME_PREFIX + apk.getName() + REPORT_EXTENSION;

        File currentDirectory = new File(System.getProperty("user.dir"));
        File reportsDirectory = new File(currentDirectory, REPORTS_DIRECTORY_NAME);
        File reportFile = new File(reportsDirectory, reportName);

        if (reportFile.exists())
            return new SaafReport(reportFile);

        return null;
    }

    private static SaafReport create(File apk) throws DecodingException {
        PackageDecoder saafDecoder = PackageDecoders.saaf();
        saafDecoder.decode(apk);

        return SaafReport.find(apk);
    }

    public static SaafReport of(File apk) {
        SaafReport report = find(apk);

        if (report != null)
            return report;

        try {
            return create(apk);
        } catch (DecodingException e) {
            e.printStackTrace();
            return null;
        }
    }
}
