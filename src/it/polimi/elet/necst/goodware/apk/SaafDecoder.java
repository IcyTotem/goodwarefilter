package it.polimi.elet.necst.goodware.apk;

import it.polimi.elet.necst.goodware.utils.FileSystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.channels.FileLockInterruptionException;

public class SaafDecoder implements PackageDecoder {
    private static final String APK_EXTENSION = ".apk";
    private static final String EXTRACTED_APKS_DIRECTORY_NAME = "bytecode";
    private static final String DECODED_CONTENT_DIRECTORY_NAME = "bytecode";
    private static final String ENCODED_CONTENT_DIRECTORY_NAME = "apk_content";
    private static final String APPS_DIRECTORY_NAME = "apps";

    // -hl (headless mode) is the no-gui mode
    // -nodb avoids using a database to store results
    // -nq avois performing quick checks (only performs taint analysis)
    // -k keeps the extracted apk content in a directory
    private static String[] options = { "-hl", "-nodb", "-nq", "-k" };

    private File outputDirectory;

    public SaafDecoder(File outputDirectory) {
        this.outputDirectory = outputDirectory;
    }

    @Override
    public DecodedPackage decode(File apkFile) throws DecodingException {
        if (!apkFile.getName().toLowerCase().endsWith(APK_EXTENSION))
            throw new DecodingException(apkFile.getAbsolutePath() + ": Invalid file type.");

        if (!apkFile.exists())
            throw new DecodingException(new FileNotFoundException(apkFile.getAbsolutePath() + " does not exist!"));

        String[] saafArguments = new String[options.length + 1];
        int i = 0;

        for (int j = 0 ; j < options.length; j++)
            saafArguments[i++] = options[j];

        saafArguments[i++] = apkFile.getAbsolutePath();

        try {
            de.rub.syssec.saaf.Main.main(saafArguments);
            return buildPackageInfo(apkFile);
        } catch (Exception e) {
            throw new DecodingException(e);
        }
    }

    private DecodedPackage buildPackageInfo(File apkFile) throws FileNotFoundException {
        final String apkName = getNameWithoutExtension(apkFile);
        final String currentDirectory = System.getProperty("user.dir");
        final File extractedDirectory = new File(currentDirectory, EXTRACTED_APKS_DIRECTORY_NAME);

        File apkDirectory = null;

        for (File file : extractedDirectory.listFiles()) {
            if (!file.isDirectory())
                continue;

            if (file.getName().startsWith(apkName)) {
                apkDirectory = file;
                break;
            }
        }

        if (apkDirectory == null)
            throw new FileNotFoundException("Cannot find decoded directory for " + apkFile.getName());

        final File decodedDirectory = new File(apkDirectory, DECODED_CONTENT_DIRECTORY_NAME);
        final File encodedDirectory = new File(apkDirectory, ENCODED_CONTENT_DIRECTORY_NAME);
        final File apkFileReference = apkFile;
        final File apkDirectoryReference = apkDirectory;

        /*
        * - apkDirectory
        *   |- decodedDirectory
        *   |  |- smaliDirectory
        *   |  |- androidManifestFile
        *   |- encodedDirectory
        *      |- classesDexFile
        * */
        return new DecodedPackage() {
            @Override
            public File getClassesDex() {
                return new File(encodedDirectory, CLASSES_DEX_FILE_NAME);
            }

            @Override
            public File getAndroidManifest() {
                return new File(decodedDirectory, ANDROID_MANIFEST_FILE_NAME);
            }

            @Override
            public File getDecodedDirectory() {
                return decodedDirectory;
            }

            @Override
            public File getSmaliDirectory() {
                return new File(decodedDirectory, SMALI_DIRECTORY_NAME);
            }

            @Override
            public File getOriginalApk() {
                return apkFileReference;
            }

            @Override
            public void dispose() {
                FileSystem.deleteDirectory(apkDirectoryReference);
                // saaf also copies the analyzed apk in ./apps
                File copiedApk = new File(new File(currentDirectory, APPS_DIRECTORY_NAME), apkFileReference.getName());
                copiedApk.delete();
            }
        };
    }

    private String getNameWithoutExtension(File file) {
        String name = file.getName();
        int dotIndex = name.lastIndexOf('.');

        if (dotIndex >= 0)
            return name.substring(0, dotIndex);

        return name;
    }
}
