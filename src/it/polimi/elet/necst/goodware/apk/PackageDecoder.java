package it.polimi.elet.necst.goodware.apk;

import java.io.File;

public interface PackageDecoder {
    DecodedPackage decode(File apkFile) throws DecodingException;
}
