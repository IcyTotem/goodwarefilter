package it.polimi.elet.necst.goodware.xml.manifest;

import it.polimi.elet.necst.goodware.xml.ParsingException;

import java.io.File;

public interface ManifestAnalyzer {
    ManifestAnalysisReport analyze(File manifestFile) throws ParsingException;
}
