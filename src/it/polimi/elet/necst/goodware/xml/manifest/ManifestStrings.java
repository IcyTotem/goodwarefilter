package it.polimi.elet.necst.goodware.xml.manifest;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Nicolo on 04/02/14.
 */
class ManifestStrings {
    public static final String PERMISSION_TAG = "uses-permission";
    public static final String APPLICATION_TAG = "application";

    public static final String LABEL_ATTRIBUTE = "android:label";
    public static final String DESCRIPTION_ATTRIBUTE = "android:description";
    public static final String NAME_ATTRIBUTE = "android:name";
    public static final String PACKAGE_ATTRIBUTE = "package";

    public static final String RECEIVE_SMS = "android.permission.RECEIVE_SMS";

    public static final String SEND_SMS = "android.permission.SEND_SMS";
    public static final String INTERNET = "android.permission.INTERNET";
    public static final String BLUETOOTH = "android.permission.BLUETOOTH";
    public static final String NFC = "android.permission.NFC";
    public static final String TRANSMIT_IR = "android.permission.TRANSMIT_IR";
    public static final String USE_SIP = "android.permission.USE_SIP";
    public static final String WRITE_SMS = "android.permission.WRITE_SMS";
    public static final String WRITE_SOCIAL_STREAM = "android.permission.WRITE_SOCIAL_STREAM";
    public static final String WRITE_APN_SETTINGS = "android.permission.WRITE_APN_SETTINGS";

    public static final List<String> COMMUNICATION_PERMISSIONS = Arrays.asList(new String[]{SEND_SMS, INTERNET, BLUETOOTH, NFC, TRANSMIT_IR, USE_SIP, WRITE_SMS, WRITE_SOCIAL_STREAM, WRITE_APN_SETTINGS});

    public static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";
}
