package it.polimi.elet.necst.goodware.xml.manifest;

import java.util.Collection;

/**
 * Created by Nicolo on 04/02/14.
 */
public interface ManifestAnalysisReport {
    String getPackageName();
    String getApplicationName();
    String getApplicationDescription();
    Collection<String> getPermissions();
}
