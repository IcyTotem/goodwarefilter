package it.polimi.elet.necst.goodware.xml.resources;

public class StringResourceParsers {
    private static DomStringResourceParser domParser;

    public static StringResourceParser domBased() {
        if (domParser != null)
            return domParser;

        domParser = new DomStringResourceParser();
        return domParser;
    }
}
