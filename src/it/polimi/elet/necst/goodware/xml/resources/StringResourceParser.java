package it.polimi.elet.necst.goodware.xml.resources;

import it.polimi.elet.necst.goodware.xml.ParsingException;

import java.io.File;

public interface StringResourceParser {
    StringResource parse(File resourceFile) throws ParsingException;
}
