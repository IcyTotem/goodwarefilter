package it.polimi.elet.necst.goodware.xml.resources;

import java.util.Collection;

public interface StringResource {
    Collection<String> getAllNames();
    String getValue(String name);
    StringResource merge(StringResource resource);
    Boolean isEmpty();
}
