package it.polimi.elet.necst.goodware.utils;

import java.io.File;

public class FileSystem {
    public static boolean deleteDirectory(File directory) {
        if (directory.exists()) {
            for (File file : directory.listFiles()) {
                if (file.isDirectory())
                    deleteDirectory(file);
                else {
                    file.delete();

                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException e) { }
                }
            }
        }

        return (directory.delete());
    }
}
