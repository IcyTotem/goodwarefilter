package it.polimi.elet.necst.goodware.csv;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CsvWriter {
    private CsvTemplate template;
    private BufferedWriter writer;
    private int fieldIndex;

    public CsvWriter(CsvTemplate template, String filename) throws IOException {
        this.template = template;
        this.writer = new BufferedWriter(new FileWriter(new File(filename)));
        this.fieldIndex = 0;
    }

    public void writeField(String value) throws IOException {
        if (fieldIndex >= template.getFieldsCount())
            newRecord();

        if (fieldIndex > 0)
            writer.write(", ");

        writer.write(value);
        fieldIndex++;
    }

    public void writeField(int value) throws IOException {
        this.writeField(Integer.valueOf(value).toString());
    }

    public void close() throws IOException {
        writer.close();
    }

    public void newRecord() throws IOException {
        for ( ; fieldIndex < template.getFieldsCount(); fieldIndex++)
            writer.write(", ");

        writer.newLine();
        fieldIndex = 0;
    }
}
