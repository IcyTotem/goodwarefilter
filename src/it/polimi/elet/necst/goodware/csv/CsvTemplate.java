package it.polimi.elet.necst.goodware.csv;

public class CsvTemplate {
    private String[] fieldNames;

    public String getFieldName(int i) {
        return fieldNames[i];
    }

    public int getFieldsCount() {
        return fieldNames.length;
    }

    public CsvTemplate(String... fields) {
        fieldNames = fields;
    }
}
