package it.polimi.elet.necst.goodware.filters;

import com.gc.android.market.api.model.Market.App;
import it.polimi.elet.necst.goodware.filters.core.MalwareFilter;
import it.polimi.elet.necst.goodware.filters.features.FeatureCollection;
import it.polimi.elet.necst.goodware.markets.GooglePlayStore;
import it.polimi.elet.necst.goodware.pipeline.ApplicationData;

import java.util.Collection;
import java.util.List;

public class PlayStorePermissionsFilter extends MalwareFilter {
    private static final String FEATURE_NAME = "Overprivileged Permissions";

    private GooglePlayStore store;

    public PlayStorePermissionsFilter() {
        store = new GooglePlayStore();
    }

    @Override
    public Confidence getConfidence() {
        return Confidence.MEDIUM;
    }

    @Override
    public boolean matches(ApplicationData applicationData) {
        String packageName = applicationData.getManifestReport().getPackageName();
        Collection<String> permissions = applicationData.getManifestReport().getPermissions();

        store.setExtendedInfo(true);
        super.clearFeatures();

        App app = store.findPackage(packageName);

        if (app == null) {
            super.features = FeatureCollection.singleton(FEATURE_NAME, 0);
            return false;
        }

        List<String> appPermissions = app.getExtendedInfo().getPermissionIdList();
        int overprivileges = 0;

        for (String permission : permissions)
            if (!appPermissions.contains(permission)) {
                if (DangerousPermissionsFilter.DANGEROUS_PERMISSIONS.contains(permission) ||
                    PotentialLeakageFilter.DATA_PERMISSIONS.contains(permission) ||
                    PotentialLeakageFilter.SENDING_PERMISSIONS.contains(permission))
                    overprivileges++;
            }

        super.features = FeatureCollection.singleton(FEATURE_NAME, overprivileges);

        return (overprivileges > 0);
    }
}
