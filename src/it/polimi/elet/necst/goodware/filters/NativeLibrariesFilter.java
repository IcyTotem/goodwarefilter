package it.polimi.elet.necst.goodware.filters;

import it.polimi.elet.necst.goodware.filters.core.GoodwareFilter;
import it.polimi.elet.necst.goodware.filters.features.FeatureCollection;
import it.polimi.elet.necst.goodware.pipeline.ApplicationData;

import java.io.File;
import java.io.FileReader;

public class NativeLibrariesFilter extends GoodwareFilter {
    private static final String FEATURE_NAME = "Contains Native Libraries";
    private static final char[] SIGNATURE = { 0x7f, 'E', 'L', 'F'};

    @Override
    public Confidence getConfidence() {
        return Confidence.LOW;
    }

    @Override
    public boolean matches(ApplicationData applicationData) {
        File directory = applicationData.getDecodedPackage().getDecodedDirectory();
        boolean nativeLibrariesPresent = containsNativeLibraries(directory);

        super.clearFeatures();
        super.features = FeatureCollection.singleton(FEATURE_NAME, nativeLibrariesPresent);

        return !nativeLibrariesPresent;
    }

    public static boolean isNativeLibrary(File file) {
        try {
            FileReader reader = new FileReader(file);
            char[] signature = new char[SIGNATURE.length];
            reader.read(signature, 0, SIGNATURE.length);
            reader.close();

            for (int i = 0; i < SIGNATURE.length; i++)
                if (signature[i] != SIGNATURE[i])
                    return false;

            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean containsNativeLibraries(File directory) {
        File[] files = directory.listFiles();

        for (File file : files) {
            if (file.isDirectory()) {
                if (containsNativeLibraries(file))
                    return true;
            }
            else if (isNativeLibrary(file))
                return true;
        }

        return false;
    }
}
