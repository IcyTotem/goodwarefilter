package it.polimi.elet.necst.goodware.filters;

import it.polimi.elet.necst.goodware.filters.core.Filter;
import it.polimi.elet.necst.goodware.filters.features.Feature;
import it.polimi.elet.necst.goodware.pipeline.ApplicationData;

import java.util.*;

public class MetaFilter {
    private List<Filter> filters;

    public Collection<Filter> getFilters() {
        return filters;
    }

    public MetaFilter() {
        filters = new ArrayList<Filter>();
    }

    public void add(Filter filter) {
        filters.add(filter);
    }

    public void remove(Filter filter) {
        filters.remove(filter);
    }

    public Map<Filter, Boolean> matchAllFilters(ApplicationData applicationData) {
        Map<Filter, Boolean> matches = new HashMap<Filter, Boolean>();

        for (Filter filter : filters)
            matches.put(filter, filter.matches(applicationData));

        return matches;
    }

    public Collection<Feature> getAllFiltersFeatures() {
        List<Feature> featureList = new ArrayList<Feature>();

        for (Filter filter : filters)
            featureList.addAll(filter.getMatchFeatures());

        return featureList;
    }

    @Deprecated
    public float applyFilters(ApplicationData applicationData) {
        float score = 0;

        for (Filter filter : filters) {
            float b = filter.getBias().equals(Filter.Bias.GOODWARE) ? GOODWARE_CLUE_WEIGHT : MALWARE_CLUE_WEIGHT;
            float m = filter.matches(applicationData) ? MATCH_WEIGHT : MISMATCH_WEIGHT;

            switch (filter.getConfidence()) {
                case HIGH:   score += HIGH_SCORE * m * b; break;
                case MEDIUM: score += MEDIUM_SCORE * m * b; break;
                case LOW:    score += LOW_SCORE * m * b;
            }

            if (score > UPPER_THRESHOLD)
                return score;

            if (score < LOWER_THRESHOLD)
                return score;
        }

        if (score < UPPER_THRESHOLD / 2)
            return score;

        return score;
    }

    private static final float HIGH_SCORE = 4;
    private static final float MEDIUM_SCORE = 2;
    private static final float LOW_SCORE = 1;

    private static final float UPPER_THRESHOLD = 8;
    private static final float LOWER_THRESHOLD = -8;

    private static final float MATCH_WEIGHT = 2;
    private static final float MISMATCH_WEIGHT = -1;

    private static final float GOODWARE_CLUE_WEIGHT = 0.8f;
    private static final float MALWARE_CLUE_WEIGHT = -1.5f;
}
