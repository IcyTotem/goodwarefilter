package it.polimi.elet.necst.goodware.filters;

import it.polimi.elet.necst.goodware.filters.core.MalwareFilter;
import it.polimi.elet.necst.goodware.filters.features.FeatureCollection;
import it.polimi.elet.necst.goodware.pipeline.ApplicationData;

import java.io.File;

public class AirpushFilter extends MalwareFilter {
    private static final String AIRPUSH_CLASS_NAME = "Airpush";
    private static final String FEATURE_NAME = "Airpush Included";

    @Override
    public Confidence getConfidence() {
        return Confidence.HIGH;
    }

    @Override
    public boolean matches(ApplicationData applicationData) {
        File smaliDirectory = applicationData.getDecodedPackage().getSmaliDirectory();
        Boolean result = containsAirpush(smaliDirectory);

        super.clearFeatures();
        super.features = FeatureCollection.singleton(FEATURE_NAME, result);

        return result;
    }

    private boolean containsAirpush(File directory) {
        if (!directory.exists())
            return false;

        for (File file : directory.listFiles()) {
            if (file.isDirectory() && containsAirpush(file))
                return true;

            if (file.getName().startsWith(AIRPUSH_CLASS_NAME))
                return true;
        }

        return false;
    }
}
