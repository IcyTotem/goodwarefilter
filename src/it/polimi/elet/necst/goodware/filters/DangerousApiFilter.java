package it.polimi.elet.necst.goodware.filters;

import it.polimi.elet.necst.goodware.filters.core.MalwareFilter;
import it.polimi.elet.necst.goodware.filters.features.Feature;
import it.polimi.elet.necst.goodware.pipeline.ApplicationData;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class DangerousApiFilter extends MalwareFilter {
    private static final String SMALI_EXTENSION = ".smali";
    private static final String FEATURE_PREFIX = "Api Call: ";

    private static final int MEDIUM_CONFIDENCE_THRESHOLD = 3;
    private static final int HIGH_CONFIDENCE_THRESHOLD = 5;

    private boolean[] alreadyFound;
    private int suspiciousLinesCount;

    public DangerousApiFilter() {
        alreadyFound = new boolean[DANGEROUS_SMALI_APIS.length];
    }

    @Override
    public Confidence getConfidence() {
        if (suspiciousLinesCount >= HIGH_CONFIDENCE_THRESHOLD)
            return Confidence.HIGH;

        if (suspiciousLinesCount >= MEDIUM_CONFIDENCE_THRESHOLD)
            return Confidence.MEDIUM;

        return Confidence.LOW;
    }

    @Override
    public boolean matches(ApplicationData applicationData) {
        this.reset();
        super.clearFeatures();

        File directory = applicationData.getDecodedPackage().getDecodedDirectory();
        boolean result = this.containsDangerousApis(directory);

        for (int i = 0; i < DANGEROUS_SMALI_APIS.length; i++)
            super.features.add(new Feature(FEATURE_PREFIX + DANGEROUS_SMALI_APIS[i], alreadyFound[i]));

        return result;
    }

    private void reset() {
        suspiciousLinesCount = 0;
        for (int i = 0; i < alreadyFound.length; i++)
            alreadyFound[i] = false;
    }

    private boolean containsDangerousApis(File directory) {
        boolean found = false;

        for (File file : directory.listFiles()) {
            if (file.isFile() && file.getName().endsWith(SMALI_EXTENSION))
                this.analyzeSmali(file);

            if (this.getConfidence().equals(Confidence.HIGH))
                return true;

            if (file.isDirectory())
                found = found || containsDangerousApis(file);
        }

        if (found)
            return true;

        for (int i = 0; i < alreadyFound.length; i++)
            if (alreadyFound[i])
                return true;

        return false;
    }

    private void analyzeSmali(File file) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line;

            while ((line = reader.readLine()) != null) {
                for (int i = 0; i < DANGEROUS_SMALI_APIS.length; i++)
                    if (!alreadyFound[i] && line.contains(DANGEROUS_SMALI_APIS[i])) {
                        suspiciousLinesCount++;
                        alreadyFound[i] = true;
                    }
            }

            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Taken from http://www.cis.syr.edu/~wedu/Research/paper/Malware_Analysis_2013.pdf
    private static final String[] DANGEROUS_SMALI_APIS = {
            "android/content/Context;->startService",
            "android/telephony/TelephonyManager;->getSubscriberId",
            "android/telephony/TelephonyManager;->getDeviceId",
            "android/telephony/TelephonyManager;->getLine1Number",
            "android/telephony/TelephonyManager;->getSimSerialNumber",
            "android/telephony/TelephonyManager;->getCellLocation",
            "android/content/Intent;->setDataAndType",
            "android/content/Intent;->setType",
            "android/app/ActivityManager;->getRunningServices",
            "android/app/ActivityManager;->getMemoryInfo",
            "android/app/ActivityManager;->restartPackage",
            "android/content/pm/PackageManager;->getInstalledPackages",
            "java/lang/System;->loadLibrary"
    };
}
