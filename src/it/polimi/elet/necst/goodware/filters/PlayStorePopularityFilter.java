package it.polimi.elet.necst.goodware.filters;

import com.gc.android.market.api.model.Market.App;
import it.polimi.elet.necst.goodware.filters.core.GoodwareFilter;
import it.polimi.elet.necst.goodware.filters.features.Feature;
import it.polimi.elet.necst.goodware.filters.features.FeatureCollection;
import it.polimi.elet.necst.goodware.markets.GooglePlayStore;
import it.polimi.elet.necst.goodware.pipeline.ApplicationData;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class PlayStorePopularityFilter extends GoodwareFilter {
    private static volatile Map<String, Collection<App>> popularAppsByCategory;

    private GooglePlayStore store;
    private boolean topCategoryAnalysisEnabled;
    private int topCount;

    public PlayStorePopularityFilter() {
        this.setTopCount(100);
        this.setTopCategoryAnalysisEnabled(false);
        this.store = new GooglePlayStore();
    }

    @Override
    public Confidence getConfidence() {
        return Confidence.HIGH;
    }

    @Override
    public synchronized boolean matches(ApplicationData applicationData) {
        if (popularAppsByCategory == null) {
            popularAppsByCategory = new HashMap<String, Collection<App>>();
        }

        String packageName = applicationData.getManifestReport().getPackageName();

        super.clearFeatures();

        boolean popular = isPopular(packageName);
        boolean top = isTop(packageName);

        return popular || top;
    }

    private boolean isPopular(String packageName) {
        store.setExtendedInfo(true);

        App target = store.findPackage(packageName);
        boolean result = false;

        if (target != null) {
            if (Float.valueOf(target.getRating()) >= MIN_RATING && target.getRatingsCount() >= MIN_RATING_COUNT)
                result = true;

            if (target.getExtendedInfo().getDownloadsCount() >= MIN_DOWNLOADS_COUNT)
                result = true;

            super.features = FeatureCollection.map(
                new String[] { FEATURE_RATING, FEATURE_RATINGS_COUNT, FEATURE_DOWNLOADS },
                new Object[] { target.getRating(), target.getRatingsCount(), target.getExtendedInfo().getDownloadsCount() }
            );
        }
        else
            super.features = FeatureCollection.map(
                    new String[] { FEATURE_RATING, FEATURE_RATINGS_COUNT, FEATURE_DOWNLOADS },
                    new Object[] { 0, 0, 0 }
            );

        return result;
    }

    private boolean isTop(String packageName) {
        if (this.isTopCategoryAnalysisEnabled()) {
            store.setExtendedInfo(false);
            store.setDefaultEntriesCount(this.getTopCount());

            for (String category : GooglePlayStore.APP_CATEGORIES) {
                if (!popularAppsByCategory.containsKey(category)) {
                    popularAppsByCategory.put(category, store.searchByCategory(category));
                }

                int rank = 1;

                for (App app : popularAppsByCategory.get(category)) {
                    if (app.getPackageName().equals(packageName)) {
                        super.features.add(new Feature(FEATURE_TOP_CATEGORY, category));
                        super.features.add(new Feature(FEATURE_TOP_POSITION, rank));
                        return true;
                    }
                    rank++;
                }
            }
        }

        super.features.add(new Feature(FEATURE_TOP_CATEGORY, ""));
        super.features.add(new Feature(FEATURE_TOP_POSITION, Integer.MAX_VALUE));
        return false;
    }

    public int getTopCount() {
        return topCount;
    }

    public void setTopCount(int value) {
        if (value > 0)
            this.topCount = value;
    }

    public boolean isTopCategoryAnalysisEnabled() {
        return topCategoryAnalysisEnabled;
    }

    public void setTopCategoryAnalysisEnabled(boolean value) {
        this.topCategoryAnalysisEnabled = value;
    }

    private static final String FEATURE_RATING = "Google Play Rating";
    private static final String FEATURE_RATINGS_COUNT = "Google Play Ratings Count";
    private static final String FEATURE_DOWNLOADS = "Google Play Downloads";
    private static final String FEATURE_TOP_POSITION = "Google Play Category Ranking";
    private static final String FEATURE_TOP_CATEGORY = "Google Play Category";

    private static final float MIN_RATING = 4.0f;
    private static final int MIN_RATING_COUNT = 500;
    private static final int MIN_DOWNLOADS_COUNT = 10000;

}
