package it.polimi.elet.necst.goodware.filters.core;

import it.polimi.elet.necst.goodware.filters.features.Feature;
import it.polimi.elet.necst.goodware.filters.features.FeatureGatherer;

import java.util.Collection;

public abstract class GoodwareFilter extends FeatureGatherer implements Filter {
    @Override
    public Bias getBias() {
        return Bias.GOODWARE;
    }

    @Override
    public Collection<Feature> getMatchFeatures() {
        return super.features;
    }
}
