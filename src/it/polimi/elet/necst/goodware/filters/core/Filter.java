package it.polimi.elet.necst.goodware.filters.core;

import it.polimi.elet.necst.goodware.filters.features.Feature;
import it.polimi.elet.necst.goodware.pipeline.ApplicationData;

import java.util.Collection;

public interface Filter {
    public enum Bias {
        GOODWARE,
        MALWARE
    }

    public enum Confidence {
        HIGH,
        MEDIUM,
        LOW
    }

    Confidence getConfidence();
    Bias getBias();
    Collection<Feature> getMatchFeatures();

    boolean matches(ApplicationData applicationData);
}
