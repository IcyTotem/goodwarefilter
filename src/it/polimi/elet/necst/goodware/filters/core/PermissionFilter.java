package it.polimi.elet.necst.goodware.filters.core;

import it.polimi.elet.necst.goodware.filters.features.Feature;
import it.polimi.elet.necst.goodware.filters.features.FeatureGatherer;

import java.util.Collection;
import java.util.List;

public abstract class PermissionFilter extends FeatureGatherer implements Filter {
    @Override
    public Collection<Feature> getMatchFeatures() {
        return super.features;
    }

    protected static boolean check(List<String> permissionList, String permission) {
        for (String permissionSuffix : permissionList)
            if (permission.endsWith(permissionSuffix))
                return true;

        return false;
    }
}
