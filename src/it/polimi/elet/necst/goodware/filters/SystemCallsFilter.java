package it.polimi.elet.necst.goodware.filters;

import it.polimi.elet.necst.goodware.apk.SaafReport;
import it.polimi.elet.necst.goodware.filters.core.MalwareFilter;
import it.polimi.elet.necst.goodware.filters.features.Feature;
import it.polimi.elet.necst.goodware.pipeline.ApplicationData;
import it.polimi.elet.necst.goodware.utils.Xml;
import org.w3c.dom.Element;

import java.io.File;

public class SystemCallsFilter extends MalwareFilter {
    private static final String FEATURE_PREFIX = "Calls a System Routine: ";

    private boolean[] commandFound;

    public SystemCallsFilter() {
        this.commandFound = new boolean[DANGEROUS_COMMANDS.length];
    }

    @Override
    public Confidence getConfidence() {
        return Confidence.HIGH;
    }

    private void reset() {
        for (int i = 0; i < commandFound.length; i++)
            commandFound[i] = false;
    }

    @Override
    public boolean matches(ApplicationData applicationData) {
        File originalApk = applicationData.getDecodedPackage().getOriginalApk();
        SaafReport report = SaafReport.of(originalApk);

        this.reset();
        super.clearFeatures();

        if (report == null)
            return false;

        report.analyzeBacktracks(new SaafReport.AnalysisTask() {
            @Override
            public void perform(Element element) {
                parseReportElement(element);
            }
        });

        boolean result = false;

        for (int i = 0; i < DANGEROUS_COMMANDS.length; i++) {
            super.features.add(new Feature(FEATURE_PREFIX + DANGEROUS_COMMANDS[i], commandFound[i]));
            if (commandFound[i])
                result = true;
        }

        return result;
    }

    private void parseReportElement(Element backtrackElement) {
        Element patternElement = Xml.getChildElement(backtrackElement, PATTERN_TAG_NAME);
        Element variableTypeElement = Xml.getChildElement(backtrackElement, VARIABLE_TYPE_TAG_NAME);
        Element valueElement = Xml.getChildElement(backtrackElement, VALUE_TAG_NAME);

        if ((patternElement == null) || (variableTypeElement == null) || (valueElement == null))
            return;

        if (patternElement.getTextContent().equals(EXTERNAL_PROGRAM_PATTERN) &&
                variableTypeElement.getTextContent().endsWith(ANONYMOUS_CONSTANT)) {
            String programName = valueElement.getTextContent();

            for (int i = 0; i < DANGEROUS_COMMANDS.length; i++)
                if (!commandFound[i] && programName.contains(DANGEROUS_COMMANDS[i]))
                    commandFound[i] = true;
        }
    }

    private static final String PATTERN_TAG_NAME = "pattern";
    private static final String VALUE_TAG_NAME = "value";
    private static final String VARIABLE_TYPE_TAG_NAME = "variable-type";

    private static final String EXTERNAL_PROGRAM_PATTERN = "Execute external program";
    private static final String ANONYMOUS_CONSTANT = "LOCAL_ANONYMOUS_CONSTANT";

    private static final String[] DANGEROUS_COMMANDS = {
            "su ", "ls ", "loadjar", "grep",
            "/sh", "/bin", "pm install", "/dev/net", "insmod",
            "rm", "mount ", "root", "/system", "stdout",
            "reboot ", "killall", "chmod ", "stderr"
    };
}
