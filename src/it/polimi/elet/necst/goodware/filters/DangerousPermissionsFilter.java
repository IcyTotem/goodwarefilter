package it.polimi.elet.necst.goodware.filters;

import it.polimi.elet.necst.goodware.filters.core.PermissionFilter;
import it.polimi.elet.necst.goodware.filters.features.Feature;
import it.polimi.elet.necst.goodware.pipeline.ApplicationData;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class DangerousPermissionsFilter extends PermissionFilter {
    private static final String FEATURE_PREFIX = "Dangerous Permission: ";

    @Override
    public Bias getBias() {
        return Bias.MALWARE;
    }

    @Override
    public Confidence getConfidence() {
        return Confidence.HIGH;
    }

    @Override
    public boolean matches(ApplicationData applicationData) {
        Collection<String> permissions = applicationData.getManifestReport().getPermissions();
        boolean result = false;

        super.clearFeatures();

        for (String dangerousPermission : DANGEROUS_PERMISSIONS) {
            boolean permissionPresent = false;

            for (String permission : permissions)
                if (permission.endsWith(dangerousPermission)) {
                    permissionPresent = true;
                    result = true;
                    break;
                }

            super.features.add(new Feature(FEATURE_PREFIX + dangerousPermission, permissionPresent));
        }

        return result;
    }

    static final List<String> DANGEROUS_PERMISSIONS = Arrays.asList(
            "BLUETOOTH_PRIVILEGED",
            "BRICK",
            "CHANGE_COMPONENT_ENABLED_STATE",
            "CLEAR_APP_USER_DATA",
            "DELETE_CACHE_FILES",
            "DELETE_PACKAGES",
            "DISABLE_KEYGUARD",
            "FACTORY_TEST",
            "INSTALL_PACKAGES",
            "MASTER_CLEAR",
            "MOUNT_FORMAT_FILESYSTEM",
            "MOUNT_UNMOUNT_FILESYSTEM",
            "PROCESS_OUTGOING_CALLS",
            "REBOOT",
            "RECEIVE_BOOT_COMPLETED",
            "WRITE_EXTERNAL_STORAGE",
            "WRITE_PROFILE",
            "WRITE_SECURE_SETTINGS"
    );
}
