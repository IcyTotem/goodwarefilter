package it.polimi.elet.necst.goodware.filters;

import it.polimi.elet.necst.goodware.apk.*;
import it.polimi.elet.necst.goodware.filters.core.MalwareFilter;
import it.polimi.elet.necst.goodware.filters.features.FeatureCollection;
import it.polimi.elet.necst.goodware.pipeline.ApplicationData;
import it.polimi.elet.necst.goodware.utils.Xml;
import org.w3c.dom.Element;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SmsNumbersFilter extends MalwareFilter {
    private static final String FEATURE_NAME = "Sends SMS to Suspicious Number(s)";
    private List<String> foundNumbers;

    public SmsNumbersFilter() {
        this.foundNumbers = new ArrayList<String>();
    }

    @Override
    public Confidence getConfidence() {
        return Confidence.HIGH;
    }

    @Override
    public boolean matches(ApplicationData applicationData) {
        File originalApk = applicationData.getDecodedPackage().getOriginalApk();
        SaafReport report = SaafReport.of(originalApk);

        foundNumbers.clear();
        super.clearFeatures();

        if (report == null) {
            super.features = FeatureCollection.singleton(FEATURE_NAME, false);
            return false;
        }

        report.analyzeBacktracks(new SaafReport.AnalysisTask() {
            @Override
            public void perform(Element element) {
                parseReportElement(element);
            }
        });

        boolean result = hasSuspiciousNumbers();

        super.features = FeatureCollection.singleton(FEATURE_NAME, result);

        return result;
    }

    private void parseReportElement(Element backtrackElement) {
        Element patternElement = Xml.getChildElement(backtrackElement, PATTERN_TAG_NAME);
        Element variableTypeElement = Xml.getChildElement(backtrackElement, VARIABLE_TYPE_TAG_NAME);
        Element valueElement = Xml.getChildElement(backtrackElement, VALUE_TAG_NAME);

        if ((patternElement == null) || (variableTypeElement == null) || (valueElement == null))
            return;

        if (patternElement.getTextContent().equals(SMS_NUMBER_PATTERN) &&
            variableTypeElement.getTextContent().equals(ANONYMOUS_CONSTANT)) {
            String number = valueElement.getTextContent();
            // Strings are enclosed in &quot;
            if ((number != null) && (number.length() > 2 * EQUOT_LENGTH)) {
                number = number.substring(EQUOT_LENGTH, number.length() - EQUOT_LENGTH);
                foundNumbers.add(number);
            }
        }
    }

    private boolean hasSuspiciousNumbers() {
        if (foundNumbers.size() == 0)
            return false;

        for (String number : foundNumbers) {
            boolean isCarrierServiceNumber = false;

            for (String prefix : CARRIER_NUMBERS_PREFIXES)
                if (number.startsWith(prefix)) {
                    isCarrierServiceNumber = true;
                    break;
                }

            if (!isCarrierServiceNumber)
                return true;
        }

        return false;
    }

    private static final String PATTERN_TAG_NAME = "pattern";
    private static final String VALUE_TAG_NAME = "value";
    private static final String VARIABLE_TYPE_TAG_NAME = "variable-type";

    private static final String SMS_NUMBER_PATTERN = "Number of a SMS message";
    private static final String ANONYMOUS_CONSTANT = "LOCAL_ANONYMOUS_CONSTANT";

    // Numbers starting with #, ##, #*, * or ** are usually employed by carriers to provide
    // instant services such as account balance and account management
    private static final String[] CARRIER_NUMBERS_PREFIXES = { "#", "*" };

    private static final int EQUOT_LENGTH = 1; // Length of the string "\"" (a single quote)
}
