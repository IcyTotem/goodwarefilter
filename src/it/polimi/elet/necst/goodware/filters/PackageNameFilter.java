package it.polimi.elet.necst.goodware.filters;

import it.polimi.elet.necst.goodware.filters.core.MalwareFilter;
import it.polimi.elet.necst.goodware.filters.features.FeatureCollection;
import it.polimi.elet.necst.goodware.pipeline.ApplicationData;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PackageNameFilter extends MalwareFilter {
    private static final String FEATURE_SHORT_NAME = "Single Package Name";
    private static final String FEATURE_DOMAIN = "Valid Domain in Package Name";
    private static final String FEATURE_TETRAGRAM = "Package Name contains Tetragrams";

    private static final String VALID_DOMAIN_LIST_NAME = "domain-list.txt";
    private static final List<Character> VOWELS = Arrays.asList('a', 'e', 'i', 'o', 'u', 'y');
    private static List<String> validDomains;

    @Override
    public Confidence getConfidence() {
        return Confidence.LOW;
    }

    @Override
    public boolean matches(ApplicationData applicationData) {
        if (validDomains == null)
            loadValidDomains();

        String packageName = applicationData.getManifestReport().getPackageName();
        String[] packageParts = packageName.split("\\.");

        boolean shortName = (packageParts.length <= 1);
        boolean validDomain = (validDomains.contains(packageParts[0].toUpperCase()));
        boolean tetragrams = false;

        for (String part : packageParts)
            if (containsConsonantNGram(part, 4)) {
                tetragrams = true;
                break;
            }

        super.clearFeatures();
        super.features = FeatureCollection.map(
            new String[] { FEATURE_SHORT_NAME, FEATURE_DOMAIN, FEATURE_TETRAGRAM },
            new Object[] { shortName, validDomain, tetragrams}
        );

        return shortName || !validDomain || tetragrams;
    }

    private static boolean containsConsonantNGram(String text, int n) {
        int counter = 0;

        for (Character c : text.toCharArray()) {
            if (Character.isLetter(c) && !VOWELS.contains(Character.toLowerCase(c)))
                counter++;
            else
                counter = 0;

            if (counter >= n)
                return true;
        }

        return false;
    }

    private static synchronized void loadValidDomains() {
        InputStream stream = PackageNameFilter.class.getResourceAsStream(VALID_DOMAIN_LIST_NAME);
        Reader reader = new InputStreamReader(stream);
        BufferedReader textReader = new BufferedReader(reader);
        String line;

        validDomains = new ArrayList<String>();

        try {
            while ((line = textReader.readLine()) != null)
                validDomains.add(line);

            textReader.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
