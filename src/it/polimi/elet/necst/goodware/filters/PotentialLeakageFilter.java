package it.polimi.elet.necst.goodware.filters;

import it.polimi.elet.necst.goodware.filters.core.PermissionFilter;
import it.polimi.elet.necst.goodware.filters.features.Feature;
import it.polimi.elet.necst.goodware.pipeline.ApplicationData;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class PotentialLeakageFilter extends PermissionFilter {
    private static final String FEATURE_PREFIX = "Can Steal Data: ";
    private static final int MEDIUM_CONFIDENCE_THRESHOLD = 2;
    private static final int HIGH_CONFIDENCE_THRESHOLD = 4;

    private int permissionCount;

    @Override
    public Bias getBias() {
        return Bias.GOODWARE;
    }

    @Override
    public Confidence getConfidence() {
        if (permissionCount >= HIGH_CONFIDENCE_THRESHOLD)
            return Confidence.HIGH;

        if (permissionCount >= MEDIUM_CONFIDENCE_THRESHOLD)
            return Confidence.MEDIUM;

        return Confidence.LOW;
    }

    @Override
    public boolean matches(ApplicationData applicationData) {
        Collection<String> permissions = applicationData.getManifestReport().getPermissions();
        boolean canReadData = false;
        boolean canSendData = false;

        super.clearFeatures();
        permissionCount = 0;

        for (String permission : permissions) {
            if (check(SENDING_PERMISSIONS, permission))
                canSendData = true;
        }

        for (String dataPermission : DATA_PERMISSIONS) {
            boolean present = false;

            for (String permission : permissions)
                if (permission.endsWith(dataPermission)) {
                    present = true;
                    break;
                }

            super.features.add(new Feature(FEATURE_PREFIX + dataPermission, present));
        }

        if (!canSendData) {
            super.clearFeatures();
            for (String dataPermission : DATA_PERMISSIONS)
                super.features.add(new Feature(FEATURE_PREFIX + dataPermission, false));
        }

        return !(canReadData && canSendData);
    }

    static final List<String> SENDING_PERMISSIONS = Arrays.asList(
            "INTERNET",
            "ACCESS_NETWORK_STATE",
            "CHANGE_NETWORK_STATE",
            "BLUETOOTH_ADMIN",
            "BLUETOOTH",
            "WRITE_APN_SETTINGS",
            "NETWORK",
            "SUBSCRIBED_FEEDS_WRITE",
            "NFC",
            "NETWORK_PROVIDER",
            "WRITE_SOCIAL_STREAM",
            "SEND_SMS",
            "USE_SIP"
    );

    static final List<String> DATA_PERMISSIONS = Arrays.asList(
            "ACCESS_FINE_LOCATION",
            "ACCESS_COARSE_LOCATION",
            "ACCESS_LOCATION_EXTRA_COMMANDS",
            "ACCESS_MOCK_LOCATION",
            "ACCESS_WIFI_STATE",
            "CAMERA",
            "CAPTURE_AUDIO_OUTPUT",
            "CAPTURE_SECURE_VIDEO_OUTPUT",
            "CAPTURE_VIDEO_OUTPUT",
            "DIAGNOSTIC",
            "DUMP",
            "GET_ACCOUNTS",
            "GET_TASKS",
            "LOCATION_HARDWARE",
            "READ_CALENDAR",
            "READ_CONTACTS",
            "READ_EXTERNAL_STORAGE",
            "READ_HISTORY_BOOKMARKS",
            "READ_PHONE_STATE",
            "READ_PROFILE",
            "READ_SMS",
            "READ_SOCIAL_STREAM",
            "READ_SYNC_SETTINGS",
            "RECEIVE_MMS",
            "RECEIVE_SMS",
            "RECORD_AUDIO"
    );
}
