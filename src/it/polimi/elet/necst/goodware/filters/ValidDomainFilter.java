package it.polimi.elet.necst.goodware.filters;

import it.polimi.elet.necst.goodware.filters.core.GoodwareFilter;
import it.polimi.elet.necst.goodware.filters.features.FeatureCollection;
import it.polimi.elet.necst.goodware.pipeline.ApplicationData;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class ValidDomainFilter extends GoodwareFilter {
    private static final String FEATURE_NAME = "Package Domain Exists";

    @Override
    public Confidence getConfidence() {
        return Confidence.MEDIUM;
    }

    @Override
    public boolean matches(ApplicationData applicationData) {
        String packageName = applicationData.getManifestReport().getPackageName();
        String[] packageParts = packageName.split("\\.");

        super.clearFeatures();

        if (packageParts.length < 2) {
            super.features = FeatureCollection.singleton(FEATURE_NAME, false);
            return false;
        }

        try {
            InetAddress inetAddress = InetAddress.getByName(packageParts[1] + "." + packageParts[0]);
            super.features = FeatureCollection.singleton(FEATURE_NAME, true);
            return true;
        } catch (UnknownHostException uhe) {
            super.features = FeatureCollection.singleton(FEATURE_NAME, false);
            return false;
        }
    }
}
