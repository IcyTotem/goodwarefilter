package it.polimi.elet.necst.goodware.filters.features;

import java.util.ArrayList;
import java.util.Collection;

public abstract class FeatureGatherer {
    protected Collection<Feature> features;

    protected void clearFeatures() {
        if (features == null)
            features = new ArrayList<Feature>();

        features.clear();
    }
}
