package it.polimi.elet.necst.goodware.filters.features;

public class Feature {
    private String name;
    private Object value;

    public Object getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public Feature(String name, Object value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
