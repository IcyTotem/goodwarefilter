package it.polimi.elet.necst.goodware.filters;

import it.polimi.elet.necst.goodware.apk.DecodingException;
import it.polimi.elet.necst.goodware.apk.PackageDecoder;
import it.polimi.elet.necst.goodware.apk.PackageDecoders;
import it.polimi.elet.necst.goodware.filters.core.MalwareFilter;
import it.polimi.elet.necst.goodware.filters.features.FeatureCollection;
import it.polimi.elet.necst.goodware.pipeline.ApplicationData;

import java.io.File;
import java.io.FileReader;

public class HiddenApkFilter extends MalwareFilter {
    private static final String FEATURE_NAME = "Hidden Apk";

    private static PackageDecoder decoder;

    private static char[][] SIGNATURES = {
        {0x50, 0x4B, 0x03, 0x04},
        {0x50, 0x4B, 0x05, 0x06},
        {0x50, 0x4B, 0x07, 0x08}
    };

    @Override
    public Confidence getConfidence() {
        return Confidence.HIGH;
    }

    @Override
    public boolean matches(ApplicationData applicationData) {
        boolean apkPresent = containsApks(applicationData.getDecodedPackage().getDecodedDirectory());

        super.clearFeatures();
        super.features = FeatureCollection.singleton(FEATURE_NAME, apkPresent);

        return apkPresent;
    }

    public static boolean isZipBased(File file) {
        try {
            FileReader reader = new FileReader(file);
            char[] signature = new char[SIGNATURES[0].length];
            reader.read(signature, 0, signature.length);
            reader.close();

            for (char[] VALID_SIGNATURE : SIGNATURES) {
                boolean match = true;

                for (int i = 0; i < VALID_SIGNATURE.length; i++)
                    if (signature[i] != VALID_SIGNATURE[i]) {
                        match = false;
                        break;
                    }

                if (match)
                    return true;
            }

            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isApk(File file) {
        if (decoder == null)
            decoder = PackageDecoders.apkTool();

        try {
            decoder.decode(file);
            return true;
        } catch (DecodingException e) {
            return false;
        }
    }

    public static boolean containsApks(File directory) {
        File[] files = directory.listFiles();

        for (File file : files) {
            if (file.isDirectory()) {
                if (containsApks(file))
                    return true;
            } else if (isZipBased(file) && isApk(file))
                return true;
        }

        return false;
    }
}
